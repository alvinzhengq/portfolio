
import "../styles/global.scss"

import React, { useEffect, useRef, useState, useReducer } from 'react'

import {
	AnimatePresence,
	domAnimation, LazyMotion, AnimateSharedLayout, m, motion
} from "framer-motion"
import Router from "next/router"
import { useRouter } from "next/router"

import NavBar from '../lib/components/NavBar'
import Loading from '../lib/components/Loading'
import Menu from '../lib/components/Menu'

import useWindowSize from '../lib/hooks/useWindowSize'
import { fadeAnimation } from '../lib/animations/hero'

function MyApp({ Component, pageProps }) {

	const [loading, setLoading] = useState(true);
	const [menuOpen, setMenuOpen] = useState(false);
	const router = useRouter();

	const hovering = useRef({
		hover: false,
		white: false
	});
	const mouseX = useRef(0);
	const mouseY = useRef(0);
	const xp = useRef(0);
	const yp = useRef(0);

	const app = useRef();
	const scrollContainer = useRef();
	const size = useWindowSize()

	const visited = useRef([]); // TODO: Check if section has been visited, if so auto play animation

	const scrollData = {
		ease: 0.08,
		current: 0,
		previous: 0,
		rounded: 0
	};

	const setBodyHeight = () => {
		document.body.style.height = `${scrollContainer.current.getBoundingClientRect().height}px`;
		document.getElementById("overlay").style.height = `${scrollContainer.current.getBoundingClientRect().height}px`;
	};

	const skewScrolling = () => {
		if (scrollContainer.current == null) return

		scrollData.current = window.scrollY; scrollData.previous += (scrollData.current - scrollData.previous) * scrollData.ease;
		scrollData.rounded = Math.round(scrollData.previous * 100) / 100;

		scrollContainer.current.style.transform = `translate3d(0, -${scrollData.rounded}px, 0)`

		requestAnimationFrame(() => skewScrolling());
	}

	useEffect(() => {
		if (typeof (document) !== "undefined") {
			setTimeout(() => setBodyHeight(), 100);
		}
	}, [typeof (document) !== "undefined" ? size.height : 0]);

	useEffect(() => {
		if (typeof (document) !== "undefined") {
			requestAnimationFrame(() => skewScrolling());
			animateCursorPrelude(mouseX, mouseY, xp, yp, hovering);

			window.addEventListener('unload', function (e) {
				window.scrollTo(0, 0)
			});
		}

		Router.events.on('routeChangeComplete', () => {
			setTimeout(setBodyHeight, 1000)
		})

		setTimeout(() => {
			setLoading(false);
		}, 2000)
	}, [])

	return (
		<>
			<div ref={app} className='app-wrap'>
				<span id="dot" className={"dot " + (menuOpen ? 'white' : '')}></span>
				<span id="circle" className={"circle " + (menuOpen ? 'white' : '')}></span>

				<AnimatePresence>
					{
						loading && (
							<>
								<Loading />
							</>
						)
					}
				</AnimatePresence>

				<NavBar setMenuOpen={setMenuOpen} menuOpen={menuOpen} />
				<AnimatePresence>
					{
						menuOpen && (
							<Menu setMenuOpen={setMenuOpen} />
						)
					}
				</AnimatePresence>

				<div ref={scrollContainer} className="scroll">
					<AnimateSharedLayout>
						<AnimatePresence exitBeforeEnter={true}>
							<motion.div
								key={router.route}
								variants={fadeAnimation}
								initial="initial"
								animate="animate"
								exit="exit"
							>
								<div className="overlay" id="overlay"></div>
								<Component {...pageProps} loading={loading} visited={visited} />
							</motion.div>
						</AnimatePresence>
					</AnimateSharedLayout>
				</div>
			</div>
		</>
	)
}

const animateCursorPrelude = (mouseX, mouseY, xp, yp, hovering) => {
	if (typeof (document) !== "undefined") {
		document.onmousemove = (e) => {
			mouseX.current = e.x;
			mouseY.current = e.y;
		}

		document.onmousedown = () => {
			document.getElementById('circle').classList.add('click');
		}
		document.onmouseup = () => {
			document.getElementById('circle').classList.remove('click');
		}

		requestAnimationFrame(() => animateCursor(mouseX, mouseY, xp, yp, hovering))
	}
}

const animateCursor = (mouseX, mouseY, xp, yp, hovering) => {
	hovering.current.hover = false;
	hovering.current.white = false;

	for (let el of document.querySelectorAll(":hover")) {
		if (el.hasAttribute('hoverable')) {
			hovering.current.hover = true;
		}

		if (el.className == "projectMain" || el.className == "projectsNav" || el.className == "menuScreen") {
			hovering.current.white = true;
		}
	}

	xp.current += ((mouseX.current - 25 - xp.current) / 5);
	yp.current += ((mouseY.current - 25 - yp.current) / 5);

	document.getElementById('circle').style.left = `${xp.current}px`;
	document.getElementById('circle').style.top = `${yp.current}px`;

	document.getElementById('dot').style.left = `${mouseX.current - 2}px`;
	document.getElementById('dot').style.top = `${mouseY.current - 2}px`;

	if (hovering.current.hover) {
		document.getElementById('circle').classList.add('hovering');

	} else {
		try {
			document.getElementById('circle').classList.remove('hovering');
		} catch (err) { }

	}

	if (hovering.current.white) {
		document.getElementById('circle').classList.add('white');
		document.getElementById('dot').classList.add('white');

	} else {
		try {
			document.getElementById('circle').classList.remove('white');
			document.getElementById('dot').classList.remove('white');
		} catch (err) { }

	}

	requestAnimationFrame(() => animateCursor(mouseX, mouseY, xp, yp, hovering))
}

export default MyApp
