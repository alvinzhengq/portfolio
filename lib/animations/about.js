
export const bioAnimation = {

    initial: i => ({
        y: "100%",
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.2) + 0.2,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.8,
        }
    })

}

export const skillAnimation = {

    initial: i => ({
        y: "100%",
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.25) + 0.4,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.75,
        }
    })

}

export const workAnimation = {

    initial: i => ({
        y: "100%",
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.25) + 0.2,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.75,
        }
    })

}