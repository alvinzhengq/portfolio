import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import { useEffect } from 'react';

import Footer from '../../lib/components/Footer'
import { Hoverable } from '../../lib/constants';
import { arrowAnimation, textAnimation } from '../../lib/animations/project'

import RightLine from '../../public/svgs/hero/right_line.svg'

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function Maple({ visited }) {
    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.scrollTo(0, 0);
            visited.current.push('hero');
        }
    }, [])

    return (
        <div className='project'>

            <div className='intro'>
                <img src="/images/maple.png" />

                <div className='bio'>
                    <h1>
                        <motion.div
                            key="1"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={0}

                        ><b>MAPLE</b> BOT</motion.div>
                    </h1>

                    <p>
                        <motion.div
                            key="2"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={1}

                        >
                            Discord minigame bot based on the AOPS Reaper game. Made using the Discord.js library and stored realtime game data on the backend using MongoDB Atlas.
                        </motion.div>
                    </p>

                    <p>
                        <motion.div
                            key="3"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={2}
                        >
                            <b>DEC</b> 2020
                        </motion.div>
                    </p>

                    <span>
                        <div className='arrow' {...Hoverable}>
                            <motion.svg
                                key="4"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={arrowAnimation}
                                width="108" height="148" fill="none" xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="54" cy="94" r="53.5" stroke="black" />
                                <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                            </motion.svg>
                        </div>
                    </span>
                </div>

                <div className='sideLine'>
                    <RightLine />
                </div>
            </div>

            <div className='description'>
                <h1>
                    <span>ABOUT THIS</span>
                    <div className='line'></div>
                    <div className='break'></div>
                    <span>PROJECT</span>
                </h1>


                <div className='bio'>
                    <Carousel
                        renderThumbs={() => { }}
                        showStatus={false}
                        renderIndicator={(onClickHandler, isSelected, index, label) => {
                            if (isSelected) {
                                return (
                                    <li style={{
                                        background: 'rgb(20,20,20)',
                                        width: 10,
                                        height: 10,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            } else {
                                return (
                                    <li style={{
                                        background: 'rgb(40,40,40)',
                                        width: 8,
                                        height: 8,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            }
                        }}
                    >
                        <div>
                            <img src="/images/gallery/maple_help.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/maple_gameplay.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/maple_hof.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/maple_clock.png"></img>
                        </div>
                    </Carousel>

                    <div className='text'>
                        <p>Maple is a Discord bot I created based on the online game Reaper by AOPS. I recreated the game as a Discord bot to allow me and my friends to easily play it in our private Discord server. The objective of the game is to reap the most amount of points, and points acumulate over time passively. But when a player reaps points, the amount resets back to 0, and starts acumulating again.</p>

                        <p>On the technical side, I created the Discord bot using the discord.js library. As for storing game info, I used the cloud database MongoDB Atlas to store the realtime game data. In order to track time, the bot will check the last reaped time in the database and return time since then. To spice the game up, I've added a karma system where users can donate their points to other users for a higher chance to gain bonus points in the future and this info is also stored in the database in a document tied to the player's Discord ID. When a user wins a game, their win is stored in a permanent section of the database, and the game data is cleared for another game.</p>

                        <p>I've designed the bot to work for infintely many servers, since each server gets its own collection in the MongoDB. So if you want to add the bot to your server, go ahead and use this link: <a {...Hoverable} href='https://discord.com/oauth2/authorize?client_id=796444079529263164&permissions=93248&scope=bot' target="_blank">Link</a>. Once added, make a channel named #maple, and add a role to yourself called "Maple Admin". Then simply run help in the #maple channel, and you're ready to play!</p>
                        <p className='links'>
                            <a {...Hoverable} href="https://gitlab.com/alvinzhengq/maple" target="_blank">Source Code</a><br />
                        </p>
                    </div>
                </div>
            </div>

            <Footer />
        </div>
    );
}

export default Maple;