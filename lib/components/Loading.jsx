
import { loadingAnimation, textAnimation } from '../animations/loading'
import { motion } from 'framer-motion'
import { useEffect, useState } from 'react'

export default function Loading() {
    let [percentage, setPercent] = useState(8);

    useEffect(() => {
        setInterval(() => {
            setPercent(oldPercent => oldPercent + 1)
        }, 14.5)
    }, [])

    return (
        <motion.div className="loading" variants={loadingAnimation} animate="animate" exit="exit">
            <motion.div className='text' variants={textAnimation} animate="animate" exit="exit" inherit={false}>
                <div>
                    {Math.min(percentage, 100)}%
                </div>
            </motion.div>
        </motion.div>
    )
}