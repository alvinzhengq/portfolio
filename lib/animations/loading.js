
export const loadingAnimation = {
    animate: {
        y: 0,
        transition: {
            ease: [.25,.1,.25,1],
            duration: 0.8,
        }
    },

    exit: {
        y: '-100vh',
        transition: {
            ease: [.25,.1,.25,1],
            duration: 0.8,
        }
    }
}

export const textAnimation = {
    animate: {
        y: 0,
        transition: {
            ease: [.25,.1,.25,1],
            duration: 0.8,
        }
    },

    exit: {
        y: '100vh',
        transition: {
            ease: [.25,.1,.25,1],
            duration: 0.8,
        }
    }
}