import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import { useEffect } from 'react';

import Footer from '../../lib/components/Footer'
import { Hoverable } from '../../lib/constants';
import { arrowAnimation, textAnimation } from '../../lib/animations/project'

import RightLine from '../../public/svgs/hero/right_line.svg'

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function Wordle({ visited }) {
    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.scrollTo(0, 0);
            visited.current.push('hero');
        }
    }, [])

    return (
        <div className='project'>

            <div className='intro'>
                <img src="/images/wordle.png" />

                <div className='bio'>
                    <h1>
                        <motion.div
                            key="1"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={0}

                        ><b>WORDLE</b> CLI</motion.div>
                    </h1>

                    <p>
                        <motion.div
                            key="2"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={1}

                        >
                            A clean command line remake of Wordle, with the full original wordlist. Made using Rust and the TUI-rs library.
                        </motion.div>
                    </p>

                    <p>
                        <motion.div
                            key="3"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={2}
                        >
                            <b>DEC</b> 2021
                        </motion.div>
                    </p>

                    <span>
                        <div className='arrow' {...Hoverable}>
                            <motion.svg
                                key="4"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={arrowAnimation}
                                width="108" height="148" fill="none" xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="54" cy="94" r="53.5" stroke="black" />
                                <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                            </motion.svg>
                        </div>
                    </span>
                </div>

                <div className='sideLine'>
                    <RightLine />
                </div>
            </div>

            <div className='description'>
                <h1>
                    <span>ABOUT THIS</span>
                    <div className='line'></div>
                    <div className='break'></div>
                    <span>PROJECT</span>
                </h1>


                <div className='bio'>
                    <Carousel
                        renderThumbs={() => { }}
                        showStatus={false}
                        renderIndicator={(onClickHandler, isSelected, index, label) => {
                            if (isSelected) {
                                return (
                                    <li style={{
                                        background: 'rgb(20,20,20)',
                                        width: 10,
                                        height: 10,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            } else {
                                return (
                                    <li style={{
                                        background: 'rgb(40,40,40)',
                                        width: 8,
                                        height: 8,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            }
                        }}
                    >
                        <div>
                            <img src="/images/gallery/wordle_cli.png"></img>
                        </div>
                    </Carousel>

                    <div className='text wordle'>
                        <p>A for fun project that I made to learn more about the Rust language and experiment with the tui-rs library. It is a simple rewrite of the Wordle on the command line using the tui-rs library for graphics.</p>

                        <p>In the future, I plan to support multiplayer by implementing a local TCP server using tokio that allows for 1v1 games, where players compete against each other to solve the Wordle first.</p>
                        <p className='links'>
                            <a {...Hoverable} href="https://gitlab.com/alvinzhengq/wordle-cli" target="_blank">Source Code</a><br />
                        </p>
                    </div>
                </div>
            </div>

            <Footer />
        </div>
    );
}

export default Wordle;