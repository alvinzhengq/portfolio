
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import { useEffect } from 'react';
import Router from 'next/router';

import SocialMedia from '../components/SocialMedia';
import { titleAnimation, lineAnimation, arrowAnimation } from '../animations/hero'
import { Hoverable } from '../constants';

import Line from '../../public/svgs/hero/line.svg'
import RightLine from '../../public/svgs/hero/right_line.svg'

function Hero({ scrollDone, loading, visited }) {

    const [objRef, inView] = useInView();
    const controls = useAnimation();

    useEffect(() => {
        if (visited.current.indexOf('hero') != -1) {
            controls.start("animate");
            return;
        }
        
        if (inView && (scrollDone || Router.asPath.split("#")[1] === '') && !loading) {
            controls.start("animate");
            visited.current.push('hero');
        }
    }, [inView, controls, loading]);

    return (
        <>
            <div className="title">
                <div className="row">
                    <h1><motion.div
                        initial="initial"
                        animate={controls}
                        variants={titleAnimation}
                        custom={0}

                        ref={objRef}
                    >ALVIN ZHENG</motion.div></h1>
                    <div className='lineContainer'>
                        <motion.div
                            initial="initial"
                            animate={controls}
                            variants={lineAnimation}
                            custom={0}

                            className="line"
                        ></motion.div>
                    </div>
                </div>

                <div className="row">
                    <div className='lineContainer'>
                        <motion.div
                            initial="initial"
                            animate={controls}
                            variants={lineAnimation}
                            custom={1}

                            className="line"
                        ></motion.div>
                    </div>
                    <h1><motion.div
                        initial="initial"
                        animate={controls}
                        variants={titleAnimation}
                        custom={1}

                        ref={objRef}
                    >SOFTWARE</motion.div></h1>
                </div>

                <div className="row">
                    <h1><motion.div
                        initial="initial"
                        animate={controls}
                        variants={titleAnimation}
                        custom={2}

                        ref={objRef}
                    >ENGINEER</motion.div></h1>
                    <div className='lineContainer'>
                        <motion.div
                            initial="initial"
                            animate={controls}
                            variants={lineAnimation}
                            custom={2}

                            className="line"
                        ></motion.div>
                    </div>
                </div>
            </div>

            <div className="bio">
                <Line />
                <p><span>01</span>&emsp;&emsp;&emsp;&emsp;&emsp;Hey! I’m Alvin Zheng, a Software Engineer. For the past 5 years, I've been developing everything from full stack applications, to security compliance tools, and native apps. Fuelled by my love for building and designing, I hope to continue to improve in my work, and see where my passion brings me next.</p>
            </div>

            <div className='heroArrow' {...Hoverable}>
                <motion.svg
                    initial="initial"
                    animate={controls}
                    variants={arrowAnimation}
                    
                    fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="54" cy="94" r="53.5" stroke="black" />
                    <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                </motion.svg>

            </div>
            <div className='sideLine'>
                <RightLine />
            </div>

            <SocialMedia />
        </>
    );
}

export default Hero;