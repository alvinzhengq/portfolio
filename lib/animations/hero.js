
export const titleAnimation = {

    initial: i => ({
        y: "100%",
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.16) + 0.5,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.8,
        }
    })

}

export const lineAnimation = {
    initial: i => ({
        y: `${200}%`,
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.16) + 0.5,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.8,
        }
    })
}

export const arrowAnimation = {
    initial: {
        style: "bottom: 8em; opacity: 0"
    },

    animate: {
        style: "bottom: 5.5vh; opacity: 1",

        transition: {
            delay: 1.6,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.6,
        }
    }
}

export const fadeAnimation = {
    initial: {
        opacity: 0,
        transition: { duration: 1.2 }
    },
    animate: {
        opacity: 1,
        transition: { duration: 0.5, delay: 0.4 }
    },
    exit: {
        opacity: 0,
        transition: { duration: 0.5 }
    }
}