import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import { useEffect } from 'react';

import Footer from '../../lib/components/Footer'
import { Hoverable } from '../../lib/constants';
import { arrowAnimation, textAnimation } from '../../lib/animations/project'

import RightLine from '../../public/svgs/hero/right_line.svg'

import "react-responsive-carousel/lib/styles/carousel.min.css";

function Incinerate({ visited }) {
    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.scrollTo(0, 0);
            visited.current.push('hero');
        }
    }, [])

    return (
        <div className='project'>

            <div className='intro'>
                <img src="/images/incinerate.png" />

                <div className='bio'>
                    <h1>
                        <motion.div
                            key="1"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={0}
                        >
                            <b>INCINERATE</b>
                        </motion.div>
                    </h1>

                    <p>
                        <motion.div
                            key="2"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={1}
                        >
                            A free and anonymous file metadata stripping service built using Express.js, deployed onto the Tor network, and rate limited using Redis.
                        </motion.div>
                    </p>

                    <p>
                        <motion.div
                            key="3"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={2}
                        >
                            <b>SEP</b> 2020
                        </motion.div>
                    </p>

                    <span>
                        <div className='arrow' {...Hoverable}>
                            <motion.svg
                                key="4"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={arrowAnimation}
                                width="108" height="148" fill="none" xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="54" cy="94" r="53.5" stroke="black" />
                                <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                            </motion.svg>
                        </div>
                    </span>
                </div>

                <div className='sideLine'>
                    <RightLine />
                </div>
            </div>

            <div className='description'>
                <h1>
                    <span>ABOUT THIS</span>
                    <div className='line'></div>
                    <div className='break'></div>
                    <span>PROJECT</span>
                </h1>


                <div className='bio'>
                    <Carousel
                        renderThumbs={() => { }}
                        showStatus={false}
                        renderIndicator={(onClickHandler, isSelected, index, label) => {
                            if (isSelected) {
                                return (
                                    <li style={{
                                        background: 'rgb(20,20,20)',
                                        width: 10,
                                        height: 10,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            } else {
                                return (
                                    <li style={{
                                        background: 'rgb(40,40,40)',
                                        width: 8,
                                        height: 8,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            }
                        }}
                    >
                        <div>
                            <img src="/images/gallery/incinerate_homepage.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/incinerate_files.png"></img>
                        </div>
                    </Carousel>

                    <div className='text'>
                        <p>Incinerate is a robust full-stack application designed to provide a free, anonymous online tool for removing metadata from media files, championing digital privacy. I developed this project to support the cause of online privacy, ensuring utmost security throughout.</p>

                        <p>The user-friendly frontend supports a wide range of media files, from images to Microsoft Office documents, allowing bulk uploads and parallel processing using asynchronous technology. The backend employs mat2 to strip metadata, storing the processed files temporarily in a public directory and ensuring their deletion upon download or after five minutes to maintain privacy.</p>

                        <p>The application is containerized with Docker for seamless deployment and is hosted on DigitalOcean. To enhance accessibility, I integrated Tor and set up a hidden service. For secure communication, I used Certbot for HTTPS certification. Additionally, Redis is utilized for daily IP rate limiting, further securing the platform.</p>

                        <p className='links'>
                            <a {...Hoverable} href="https://incinerate.alvinqz.com" target="_blank">Live Site</a>
                            <span> | </span>
                            <a {...Hoverable} href="https://gitlab.com/alvinzhengq/incinerate-backend" target="_blank">Source Code</a><br />
                        </p>
                    </div>
                </div>
            </div>

            <Footer />
        </div>
    );
}

export default Incinerate;