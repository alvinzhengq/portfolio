
export const menuAnimation = {
    initial: {
        x: '-100%',
        transition: {
            ease: [.43, 0, .28, 1],
            duration: 1,
        }
    },

    animate: {
        x: 0,
        transition: {
            ease: [.43, 0, .28, 1],
            duration: 1,
        }
    },

    exit: {
        x: '100%',
        transition: {
            ease: [.43, 0, .28, 1],
            delay: 1,
            duration: 1,
        }
    }
}

export const text = {
    initial: {
        y: '90px',
    },
    
    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.15) + 1,
            ease: [.43, 0, .28, 1],
            duration: 0.5,
        }
    }),
    
    exit: i => ({
        y: '90px',
        transition: {
            delay: i * 0.15,
            ease: [.43, 0, .28, 1],
            duration: 0.5,
        }
    }),
}