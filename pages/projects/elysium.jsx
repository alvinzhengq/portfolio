import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import { useEffect } from 'react';

import Footer from '../../lib/components/Footer'
import { Hoverable } from '../../lib/constants';
import { arrowAnimation, textAnimation } from '../../lib/animations/project'

import RightLine from '../../public/svgs/hero/right_line.svg'

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function Elysium({ visited }) {
    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.scrollTo(0, 0);
            visited.current.push('hero');
        }
    }, [])

    return (
        <div className='project'>

            <div className='intro'>
                <img src="/images/elysium.png" />

                <div className='bio'>
                    <h1>
                        <motion.div
                            key="1"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={0}

                        ><b>ELYSIUM</b> SUITE</motion.div>
                    </h1>

                    <p>
                        <motion.div
                            key="2"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={1}

                        >
                            A suite of defensive cybersecurity training tools focused on vulnerability remediation and system compliance. Built in collaboration with other CyberPatriot national finalists. Made primarily in Golang.
                        </motion.div>
                    </p>

                    <p>
                        <motion.div
                            key="3"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={2}
                        >
                            <b>NOV</b> 2020
                        </motion.div>
                    </p>

                    <span>
                        <div className='arrow' {...Hoverable}>
                            <motion.svg
                                key="4"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={arrowAnimation}
                                width="108" height="148" fill="none" xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="54" cy="94" r="53.5" stroke="black" />
                                <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                            </motion.svg>
                        </div>
                    </span>
                </div>

                <div className='sideLine'>
                    <RightLine />
                </div>
            </div>

            <div className='description'>
                <h1>
                    <span>ABOUT THIS</span>
                    <div className='line'></div>
                    <div className='break'></div>
                    <span>PROJECT</span>
                </h1>


                <div className='bio'>
                    <Carousel
                        renderThumbs={() => { }}
                        showStatus={false}
                        renderIndicator={(onClickHandler, isSelected, index, label) => {
                            if (isSelected) {
                                return (
                                    <li style={{
                                        background: 'rgb(20,20,20)',
                                        width: 10,
                                        height: 10,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            } else {
                                return (
                                    <li style={{
                                        background: 'rgb(40,40,40)',
                                        width: 8,
                                        height: 8,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            }
                        }}
                    >
                        <div>
                            <img src="/images/gallery/sarpedon_home.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/sarpedon_score.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/minos.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/minos_details.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/minos_score.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/minos_uptime.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/aeacus_score.png"></img>
                        </div>
                    </Carousel>

                    <p>As a three-time National Champion of the CyberPatriot competition, my team and I leveraged our collective expertise to develop a comprehensive suite of training tools designed for future competitors. This suite comprises two distinct software sets: vulnerability remediation training and Red vs. Blue (RVB) training.</p>

                    <p>
                        <b>Vulnerability Remediation Training:</b>
                    </p>
                    <p>Our tool, Aeacus, is tailored for teaching vulnerability remediation on both Windows and Linux operating systems. Aeacus operates as a service, checking for and verifying the remediation of various vulnerabilities, from password changes to securing services like IIS, MySQL, and Active Directory. Aeacus reports the competitor's performance to Sarpedon, a backend service that aggregates and displays this data on a public scoreboard, providing clear metrics on remediation success.</p>

                    <p>
                        <b>Red vs. Blue (RVB) Training:</b>
                    </p>
                    <p>The RVB training simulates real-world cyber attack scenarios. Competitors (blue team) secure systems against attacks from the red team, who attempt to compromise services such as HTTP websites and SMTP servers. Minos, acting as the customer, periodically checks the blue team's service uptime and health, with the results displayed on a performance scoreboard.</p>

                    <p>Our suite leverages Golang for its speed and cross-platform capabilities, underpinning both Sarpedon and Aeacus, while Python is utilized in Minos for its automation prowess and ability to simulate human interactions. This suite provides a robust platform for aspiring cybersecurity professionals to hone their skills in vulnerability remediation and incident response, preparing them for real-world challenges.</p>
                    <p className='links'>
                        <a {...Hoverable} href="https://github.com/elysium-suite" target="_blank">Github Organization</a><br />
                        <a {...Hoverable} href="https://github.com/elysium-suite/sarpedon/commits?author=alvinzhengq" target="_blank">Sarpedon Contributions</a><br />
                        <a {...Hoverable} href="https://github.com/elysium-suite/minos/commits?author=alvinzhengq" target="_blank">Minos Contributions</a><br />
                        <a {...Hoverable} href="https://github.com/elysium-suite/aeacus/pull/139" target="_blank">Aeacus Contributions</a><br />
                    </p>
                </div>
            </div>

            <Footer />
        </div>
    );
}

export default Elysium;