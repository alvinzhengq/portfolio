
import Gitlab from '../../public/svgs/gitlab.svg'
import LinkedIn from '../../public/svgs/linkedin.svg'
import Github from '../../public/svgs/github.svg'

import { Hoverable } from '../constants'

function SocialMedia() {
    return (
        <div className="socialMedia">
            <a {...Hoverable} href='https://github.com/alvinzhengq' target="_blank"><Github /></a>
            
            <a {...Hoverable} href='https://gitlab.com/alvinzhengq/' target="_blank"><Gitlab /></a>

            <a {...Hoverable} href='https://www.linkedin.com/in/alvin-zheng-6335a8287/' target="_blank"><LinkedIn /></a>
        </div>
    );
}

export default SocialMedia;