
import { useEffect, useRef, useState } from 'react'

import HeroSection from '../lib/sections/Hero'
import ProjectSection from '../lib/sections/Projects'
import AboutSection from '../lib/sections/About'

import Footer from '../lib/components/Footer'

export default function Home({ loading, visited }) {

  const Hero = useRef();
  const Projects = useRef();
  const About = useRef();

  let [scrollDone, setScrollDone] = useState(false);

  useEffect(() => {
    if (typeof (document) !== "undefined") {
      setTimeout(() => { setScroll(Hero, Projects, About); setScrollDone(true); }, 200)
    }
  }, [typeof (document) !== "undefined" ? window.location.hash.substring(1) : ''])

  return (
    <div>
      <section ref={Hero} className='hero'>
        <HeroSection scrollDone={scrollDone} loading={loading} visited={visited} />
        <p className='sectionLabel'><span>01</span><br /><b>HOME</b></p>
      </section>

      <section ref={Projects} className='projects'>
        <ProjectSection scrollDone={scrollDone} loading={loading} visited={visited} />
        <p className='sectionLabel'><span>02</span><br /><b>PERSONAL</b> PROJECTS</p>
      </section>

      <section ref={About} className='about'>
        <AboutSection scrollDone={scrollDone} loading={loading} visited={visited} />
        <p className='sectionLabel'><span>03</span><br /><b>ABOUT</b> ME</p>
      </section>

      <Footer />
    </div>
  )
}

const setScroll = (Hero, Projects, About) => {
  if (window.location.href.indexOf('#') == -1) {
    return;
  }
  let scrollTo = window.location.hash.substring(1);

  switch (scrollTo) {
    case "":
      window.scrollTo(0, Hero.current.getBoundingClientRect().top + window.scrollY);
      break;

    case "projects":
      window.scrollTo(0, Projects.current.getBoundingClientRect().top + window.scrollY);
      break;

    case "about":
      console.log(scrollTo)
      window.scrollTo(0, About.current.getBoundingClientRect().top + window.scrollY);
      break;

    default:
      break;
  }
}
