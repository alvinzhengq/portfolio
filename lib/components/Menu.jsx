
import { motion } from "framer-motion";
import Link from 'next/link'

import { Hoverable } from '../constants'
import { menuAnimation, text } from '../animations/menu'

import Arrow from '../../public/svgs/navbar/arrow.svg'

function Menu({ setMenuOpen }) {
    return (
        <motion.div
            initial="initial"
            animate="animate"
            exit="exit"
            variants={menuAnimation}
            className="menuScreen"
        >
            <h1>
                <Arrow />
                <motion.div {...Hoverable} variants={text} custom={0} onClick={() => setMenuOpen(false)}>
                    <Link href="/#">HOME</Link>
                </motion.div>
                <Arrow />
            </h1>
            <h1>
                <Arrow />
                <motion.div {...Hoverable} variants={text} custom={1} onClick={() => setMenuOpen(false)}>
                    <Link href="/#projects">PROJECTS</Link>
                </motion.div>
                <Arrow />
            </h1>
            <h1>
                <Arrow />
                <motion.div {...Hoverable} variants={text} custom={1} onClick={() => setMenuOpen(false)}>
                    <Link href="/#about">ABOUT</Link>
                </motion.div>
                <Arrow />
            </h1>
        </motion.div>
    );
}

export default Menu;