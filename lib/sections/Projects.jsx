import {
    ButtonBack,
    ButtonNext,
    CarouselProvider,
    Slide,
    Slider,
} from 'pure-react-carousel';
import { useState, useEffect } from 'react';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import Router from 'next/router';

import { Hoverable } from '../constants'
import { textAnimation } from '../animations/projects'

import Arrow from '../../public/svgs/projects/arrow.svg'
import InnerDot from '../../public/svgs/projects/inner_dot.svg'
import OuterDot from '../../public/svgs/projects/outer_circle.svg'

import 'pure-react-carousel/dist/react-carousel.es.css';
import Link from 'next/link';

function Projects({ scrollDone, loading, visited }) {

    let [curSlide, setSlide] = useState(0);
    const [objRef, inView] = useInView();
    const controls = useAnimation();

    useEffect(() => {
        if (visited.current.indexOf('projects') != -1) {
            controls.start("animate");
            return;
        }

        if (inView && (scrollDone || Router.asPath.split("#")[1] === 'projects') && !loading) {
            controls.start("animate");
            visited.current.push('projects');
        }
    }, [inView, controls, loading]);

    return (
        <>
            <div className="projectMain">
                <CarouselProvider
                    visibleSlides={1}
                    totalSlides={4}
                    step={1}
                    naturalSlideWidth={1}
                    naturalSlideHeight={1}
                    dragEnabled={false}
                >
                    <Slider>
                        <Slide index={0}>
                            <div className='slideContainer'>
                                <div className='description'>
                                    <h1 ref={objRef}>
                                        <motion.div
                                            initial="initial"
                                            animate={controls}
                                            variants={textAnimation}
                                            custom={0}

                                        ><b>CASH</b>FLOW</motion.div>
                                    </h1>
                                    <p>
                                        <motion.div
                                            initial="initial"
                                            animate={controls}
                                            variants={textAnimation}
                                            custom={1}

                                        >
                                            Full-stack banking/eCommerce application with a fully functional JWT based authentication system, real time transactions, and live auctioning platform. Made using Next.js + MongoDB.
                                        </motion.div>
                                    </p>
                                    <h2>
                                        <motion.div
                                            initial="initial"
                                            animate={controls}
                                            variants={textAnimation}
                                            custom={2}
                                            {...Hoverable}

                                        ><div>—</div><Link scroll={false} href={'/projects/cashflow'}>LEARN MORE.</Link></motion.div>
                                    </h2>
                                </div>
                                <motion.img src='/images/cashflow.png' />
                            </div>
                        </Slide>

                        <Slide index={1}>
                            <div className='slideContainer'>
                                <div className='description'>
                                    <h1><b>ELYSIUM</b> SUITE</h1>
                                    <p>A suite of defensive cybersecurity training tools focused on vulnerability remediation and system compliance built primarily in Go & Rust, in collaboration with other CyberPatriot national finalists.</p>
                                    <h2 {...Hoverable} ><div>—</div><Link scroll={false} href={'/projects/elysium'}>LEARN MORE.</Link></h2>
                                </div>
                                <img src='/images/elysium.png' />
                            </div>
                        </Slide>

                        <Slide index={2}>
                            <div className='slideContainer'>
                                <div className='description'>
                                    <h1><b>BLANK</b> CHAT</h1>
                                    <p>A decentralized e2e encrypted native chat application routed over the Tor network. Built using websocket technology, JWT authentication, and Express.js + MongoDB backend.</p>
                                    <h2 {...Hoverable} ><div>—</div><Link scroll={false} href={'/projects/blank'}>LEARN MORE.</Link></h2>
                                </div>
                                <img src='/images/blank.png' />
                            </div>
                        </Slide>

                        <Slide index={3}>
                            <div className='slideContainer'>
                                <div className='description'>
                                    <h1><b>INCINERATE</b></h1>
                                    <p>A free and anonymous metadata stripping service built using Express.js, deployed onto the Tor network, and rate limited using Redis.</p>
                                    <h2 {...Hoverable} ><div>—</div><Link scroll={false} href={'/projects/incinerate'}>LEARN MORE.</Link></h2>
                                </div>
                                <img src='/images/incinerate.png' />
                            </div>
                        </Slide>
                    </Slider>


                    <div className="projectsNav">
                        <div className='control buttons'>
                            <ButtonBack {...Hoverable} onClick={() => {
                                setSlide(Math.max(0, curSlide - 1))
                            }}>
                                <Arrow />
                            </ButtonBack>
                            <ButtonNext {...Hoverable} onClick={() => {
                                setSlide(Math.min(3, curSlide + 1))
                            }}>
                                <Arrow />
                            </ButtonNext>
                        </div>

                        <div className='control dots'>
                            <div className='nav_dot'>
                                {curSlide == 0 && (
                                    <motion.div layoutId='outer_dot'>
                                        <OuterDot className="outer_dot" />
                                    </motion.div>
                                )}
                                <InnerDot />
                            </div>
                            <div className='nav_dot'>
                                {curSlide == 1 && (
                                    <motion.div layoutId='outer_dot'>
                                        <OuterDot className="outer_dot" />
                                    </motion.div>
                                )}
                                <InnerDot />
                            </div>
                            <div className='nav_dot'>
                                {curSlide == 2 && (
                                    <motion.div layoutId='outer_dot'>
                                        <OuterDot className="outer_dot" />
                                    </motion.div>
                                )}
                                <InnerDot />
                            </div>
                            <div className='nav_dot'>
                                {curSlide == 3 && (
                                    <motion.div layoutId='outer_dot'>
                                        <OuterDot className="outer_dot" />
                                    </motion.div>
                                )}
                                <InnerDot />
                            </div>
                        </div>
                    </div>
                </CarouselProvider>
            </div>
        </>
    );
}

export default Projects;