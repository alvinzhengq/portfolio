
import { AnimatePresence, motion } from 'framer-motion';
import { useEffect, useRef } from 'react';
import { useRouter } from 'next/router';

import { Hoverable } from '../constants'
import { navbarClosed, navbarOpen } from '../animations/navbar'

import MenuIcon from '../../public/svgs/navbar/menu.svg'
import ClosedIcon from '../../public/svgs/navbar/close.svg'
import Link from 'next/link';

function NavBar({ menuOpen, setMenuOpen }) {
    const navbar = useRef();
    const router = useRouter();

    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.onscroll = (e) => {
                if (navbar.current === null) return;

                if (window.scrollY > 0) {
                    navbar.current.classList.add("bg");
                } else {
                    try {
                        navbar.current.classList.remove("bg");
                    } catch (error) { }
                }
            }
        }
    }, [])

    return (
        <AnimatePresence exitBeforeEnter={true}>
            {
                !menuOpen ?
                    (
                        <motion.div ref={navbar} variants={navbarClosed} initial="initial" animate="animate" exit="exit" key="closed" className="navbar">
                            <Link href={router.asPath.includes('/projects') ? '/#projects' : '/'}>
                                <div {...Hoverable} className="icon">
                                    <p className="top">ALVIN</p>
                                    <p className="bot">ZHENG</p>
                                </div>
                            </Link>

                            <div {...Hoverable} className='menu' onClick={() => {
                                navbar.current.style['background-color'] = 'transparent'
                                navbar.current.style['box-shadow'] = null
                                setMenuOpen(isOpen => !isOpen)
                            }}>
                                <div className='menuIcon'><MenuIcon /></div>
                                <p>MENU</p>
                            </div>
                        </motion.div>
                    )

                    :

                    (
                        <motion.div variants={navbarOpen} initial="initial" animate="animate" exit="exit" key="open" className="navbar white">
                            <Link href={router.asPath.includes('/projects') ? '/#projects' : '/'}>
                                <div {...Hoverable} className="icon">
                                    <p className="top">ALVIN</p>
                                    <p className="bot">ZHENG</p>
                                </div>
                            </Link>

                            <div {...Hoverable} className='menu' onClick={() => setMenuOpen(isOpen => !isOpen)}>
                                <ClosedIcon />
                                <p>CLOSE</p>
                            </div>
                        </motion.div>
                    )
            }
        </AnimatePresence>
    );
}

export default NavBar;