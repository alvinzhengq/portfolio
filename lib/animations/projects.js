export const textAnimation = {

    initial: i => ({
        y: "100%",
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.3) + 0.2,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.8,
        }
    })

}