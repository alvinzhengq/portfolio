export const textAnimation = {

    initial: i => ({
        y: "100%",
    }),

    animate: i => ({
        y: 0,

        transition: {
            delay: (i * 0.3) + 0.6,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.8,
        }
    })

}

export const arrowAnimation = {
    initial: {
        y: "-100%",
        opacity: 0
    },

    animate: {
        y: "0%",
        opacity: 1,

        transition: {
            delay: 1.9,
            ease: [0.215, 0.61, 0.355, 1],
            duration: 0.6,
        }
    }
}