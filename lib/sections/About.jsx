
import { useEffect } from 'react';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import Router from 'next/router';

import { Hoverable } from '../constants'
import { bioAnimation, skillAnimation, workAnimation } from '../animations/about'

function About({ scrollDone, loading, visited }) {

    const [bioRef, bioInView] = useInView();
    const bioControl = useAnimation();

    const [skillRef, skillInView] = useInView();
    const skillControl = useAnimation();

    const [workRef, workInView] = useInView();
    const workControl = useAnimation();

    useEffect(() => {
        if (visited.current.indexOf('bio') != -1) {
            bioControl.start("animate");
            return;
        }

        if (bioInView && (scrollDone || Router.asPath.split("#")[1] === 'projects') && !loading) {
            bioControl.start("animate");
            visited.current.push('bio');
        }
    }, [bioInView, bioControl, loading]);

    useEffect(() => {
        if (visited.current.indexOf('skill') != -1) {
            skillControl.start("animate");
            return;
        }

        if (skillInView && (scrollDone || Router.asPath.split("#")[1] === 'projects') && !loading) {
            skillControl.start("animate");
            visited.current.push('skill');
        }
    }, [skillInView, skillControl, loading]);

    useEffect(() => {
        if (visited.current.indexOf('work') != -1) {
            workControl.start("animate");
            return;
        }

        if (workInView && (scrollDone || Router.asPath.split("#")[1] === 'projects') && !loading) {
            workControl.start("animate");
            visited.current.push('work');
        }
    }, [workInView, workControl, loading]);

    return (
        <>
            <div className="bio">
                <h1>
                    <motion.div
                        initial="initial"
                        animate={bioControl}
                        variants={bioAnimation}
                        custom={0}
                    >
                        A BIT MORE <b>ABOUT ME</b>
                    </motion.div>
                </h1>

                {/* <h2>
                    <motion.div
                        initial="initial"
                        animate={bioControl}
                        variants={bioAnimation}
                        custom={1}
                    >
                        <span>“</span>I’m Alvin Zheng, a Software Engineer who loves to work on projects that have a profound impact on the world.<span>“</span>
                    </motion.div>
                </h2>

                <div className="bio-p">
                    <p ref={bioRef}>
                        <motion.div
                            initial="initial"
                            animate={bioControl}
                            variants={bioAnimation}
                            custom={2}
                        >
                            I began my journey into the world of programming and design when I was 10 years old, and learned to write my first line of code in Java. It was through this very passion that I discovered my love for creating.
                        </motion.div>
                    </p>
                    <p>
                        <motion.div
                            initial="initial"
                            animate={bioControl}
                            variants={bioAnimation}
                            custom={3}
                        >
                            Today I am only 16 years old, but I am still very passionate about my work nonetheless, and it is my hope that I can continue to pursue my passion in the future. Though my goal will always remain the same: to impact the world in a profound way through my work.
                        </motion.div>
                    </p>
                </div> */}
            </div>

            <hr />

            <div className="skillset">
                <div className="row">
                    <div className="list">
                        <h2 ref={bioRef}>PROGRAMMING SKILLSET</h2>

                        <p>
                            <motion.div
                                initial="initial"
                                animate={skillControl}
                                variants={skillAnimation}
                                custom={0}
                            >
                                HTML5 & CSS3<span>  /  </span>Node.js<span>  /  </span>Rust<span>  /  </span>Golang<span>  /  </span>C++<span>  /  </span>Python<span>  /  </span>Java<span>  /  </span>SQL<span>  /  </span>Dart
                            </motion.div>
                        </p>
                    </div>

                    <div className="list">
                        <h2>FRAMEWORKS I USE</h2>

                        <p>
                            <motion.div
                                initial="initial"
                                animate={skillControl}
                                variants={skillAnimation}
                                custom={1}
                            >
                                React.js<span>  /  </span>Next.js<span>  /  </span>Electron.js<span>  /  </span>Discord.js/py<span>  /  </span>Flutter  / Express.js<span>  /  </span>OpenGL<span>  /  </span>Gin
                            </motion.div>
                        </p>
                    </div>
                </div>

                <div className="row">
                    <div className="list">
                        <h2 ref={skillRef}>TECHNOLOGIES/TOOLS I USE</h2>

                        <p>
                            <motion.div
                                initial="initial"
                                animate={skillControl}
                                variants={skillAnimation}
                                custom={2}
                            >
                                Docker<span>  /  </span>MongoDB<span>  /  </span>PostgreSQL<span>  /  </span>Redis<span>  /  </span>Nginx<span>  /  </span>Apache<span>  /  </span>Webpack<span>  /  </span>VSCode<span>  /  </span>Github<span>  /  </span>AWS<span>  /  </span>Figma
                            </motion.div>
                        </p>
                    </div>
                </div>

                <img src="/images/circles.jpg" />
            </div>

            <hr />

            <div className="work">
                <h1>WORK EXPERIENCE</h1>

                <div className="job">
                    <motion.div
                        initial="initial"
                        animate={workControl}
                        variants={workAnimation}
                        custom={0}
                    >
                        <p>Research Assistant - <b>MIT CS & AI Laboratory</b></p>
                        <li>January 2024 - Present</li>
                    </motion.div>
                </div>

                <div className="job">
                    <motion.div
                        initial="initial"
                        animate={workControl}
                        variants={workAnimation}
                        custom={0}
                    >
                        <p>Mobile Application Developer - <b>University of California - San Diego</b></p>
                        <li>November 2019 - August 2023</li>
                    </motion.div>
                </div>

                <div className="job" ref={workRef}>
                    <motion.div
                        initial="initial"
                        animate={workControl}
                        variants={workAnimation}
                        custom={1}
                    >
                        <p>Cellphone Repair Technician - <b>Swift Repairs LLC</b></p>
                        <li>June 2020 - June 2023</li>
                    </motion.div>
                </div>
            </div>

            <a {...Hoverable} className="button" href='/resume.pdf' target="_blank">
                <motion.div
                    initial="initial"
                    animate={workControl}
                    variants={workAnimation}
                    custom={2}
                >
                    <div className='circle'>
                        <img src="/images/download.png" />

                    </div>

                    <p>Download Resume</p>
                </motion.div>
            </a>
        </>
    );
}

export default About;
