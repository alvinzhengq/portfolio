
function Footer() {
    return (
        <div className="footer">
            <hr />

            <div className="content">
                <div className="contact">
                    <h1>Contact<br />Information —</h1>

                    <p>You can reach me through Email, Phone, or Discord. But I will most likely respond the fastest via. Discord.</p>
                    <p>
                        E:  <span>alvin.zheng.q@gmail.com</span><br />
                        P:  <span>+1 858-267-9912</span><br />
                        D:  <span>Cox#0008</span>
                    </p>
                </div>

                <div className="madeWith">
                    <h1>Made With —</h1>

                    <p>
                        This website was made from scratch using Next.js, and Framer Motion, and designed using Figma.
                    </p>
                    <p>
                        Feel free to check out the source code here: https://gitlab.com/alvinzhengq/portfolio, or reach out to me with any questions regarding the development process.
                    </p>
                    <p>
                        Please do not copy or redistrbute my code without my permission, you can reach out to me via the listed contact information.
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Footer;