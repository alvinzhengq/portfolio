import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import { useEffect } from 'react';

import Footer from '../../lib/components/Footer'
import { Hoverable } from '../../lib/constants';
import { arrowAnimation, textAnimation } from '../../lib/animations/project'

import RightLine from '../../public/svgs/hero/right_line.svg'

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function TRI({ visited }) {
    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.scrollTo(0, 0);
            visited.current.push('hero');
        }
    }, [])

    return (
        <div className='project'>

            <div className='intro'>
                <img src="/images/blank.png" />

                <div className='bio'>
                    <h1>
                        <motion.div
                            key="1"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={0}

                        ><b>BLANK</b> CHAT</motion.div>
                    </h1>

                    <p>
                        <motion.div
                            key="2"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={1}

                        >
                            A decentralized e2e encrypted native chat application routed over the Tor network. Built using websocket technology, JWT authentication, and Express.js + MongoDB backend.
                        </motion.div>
                    </p>

                    <p>
                        <motion.div
                            key="3"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={2}
                        >
                            <b>AUG</b> 2020
                        </motion.div>
                    </p>

                    <span>
                        <div className='arrow' {...Hoverable}>
                            <motion.svg
                                key="4"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={arrowAnimation}
                                width="108" height="148" fill="none" xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="54" cy="94" r="53.5" stroke="black" />
                                <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                            </motion.svg>
                        </div>
                    </span>
                </div>

                <div className='sideLine'>
                    <RightLine />
                </div>
            </div>

            <div className='description'>
                <h1>
                    <span>ABOUT THIS</span>
                    <div className='line'></div>
                    <div className='break'></div>
                    <span>PROJECT</span>
                </h1>


                <div className='bio'>
                    <Carousel
                        renderThumbs={() => { }}
                        showStatus={false}
                        renderIndicator={(onClickHandler, isSelected, index, label) => {
                            if (isSelected) {
                                return (
                                    <li style={{
                                        background: 'rgb(20,20,20)',
                                        width: 10,
                                        height: 10,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            } else {
                                return (
                                    <li style={{
                                        background: 'rgb(40,40,40)',
                                        width: 8,
                                        height: 8,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            }
                        }}
                    >
                        <div>
                            <img src="/images/gallery/blank_register.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/blank_login.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/blank_localpass.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/blank_verify.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/blank_socket.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/blank_settings.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/blank_data.png"></img>
                        </div>
                    </Carousel>

                    <div className='text'>
                        <p>During quarantine, I developed Blank, a privacy-focused chat client inspired by Discord with end-to-end encryption (E2EE). The frontend, built with Electron.js, requires users to create a master password for local data security, register with hashed and salted passwords, and authenticate via JWT. Data transactions occur over a WebSocket server, with users disconnecting on server or internet outages. The client code is obfuscated and includes anti-tampering measures.</p>

                        <p>The backend utilizes MongoDB for user data, Redis for temporary registration data, and Express.js for server logic. E2EE is achieved with RSA keys, stored on a key server. Chat messages are temporarily stored and encrypted locally using AES, ensuring forward secrecy and semi-decentralized storage.</p>

                        <p>Currently, Blank has functional registration, WebSocket authentication, basic server-client event handling, and completed encryption features, including the key server and local storage. Future plans include direct messaging with key exchanges, server chats with channel functionality, and implementing the Signal protocol for large-scale servers. Blank is a step forward in secure, private communication.</p>

                        <p className='links'>
                            <a {...Hoverable} href="https://gitlab.com/alvinzhengq/blank-react" target="_blank">Client Source Code</a>
                            <span> | </span>
                            <a {...Hoverable} href="https://gitlab.com/alvinzhengq/blank" target="_blank">Backend Source Code</a><br />
                        </p>
                    </div>
                </div>
            </div>

            <Footer />
        </div>
    );
}

export default TRI;