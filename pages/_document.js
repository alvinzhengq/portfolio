import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html>
      <Head />
      <head>
          <title>Alvin Zheng</title>
      </head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}