
export const navbarClosed = {
    initial: {
        opacity: 0,
    },

    animate: {
        opacity: 1,
        transition: {
            ease: [.43, 0, .28, 1],
            delay: 1.4,
            duration: 0.8,
        }
    },

    exit: {
        opacity: 0,
        transition: {
            ease: [.43, 0, .28, 1],
            duration: 0.8,
        }
    }
}


export const navbarOpen = {
    initial: {
        opacity: 0,
    },

    animate: {
        opacity: 1,
        transition: {
            ease: [.43, 0, .28, 1],
            delay: 0.9,
            duration: 0.8,
        }
    },

    exit: {
        opacity: 0,
        transition: {
            ease: [.43, 0, .28, 1],
            duration: 0.8,
        }
    }
}