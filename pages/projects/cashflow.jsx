import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';
import { useEffect } from 'react';

import Footer from '../../lib/components/Footer'
import { Hoverable } from '../../lib/constants';
import { arrowAnimation, textAnimation } from '../../lib/animations/project'

import RightLine from '../../public/svgs/hero/right_line.svg'

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

function CashFlow({ visited }) {
    useEffect(() => {
        if (typeof (document) !== "undefined") {
            window.scrollTo(0, 0);
            visited.current.push('hero');
        }
    }, [])

    return (
        <div className='project'>

            <div className='intro'>
                <motion.img src="/images/cashflow.png" />

                <div className='bio'>
                    <h1>
                        <motion.div
                            key="1"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={0}

                        ><b>CASH</b>FLOW</motion.div>
                    </h1>

                    <p>
                        <motion.div
                            key="2"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={1}

                        >
                            Full-stack banking/eCommerce application with a fully functional JWT based authentication system, real time transactions, and transaction requests. Made using Next.js, and MongoDB.
                        </motion.div>
                    </p>

                    <p>
                        <motion.div
                            key="3"
                            initial="initial"
                            animate="animate"
                            exit="exit"
                            variants={textAnimation}
                            custom={2}
                        >
                            <b>OCT</b> 2021
                        </motion.div>
                    </p>

                    <span>
                        <div className='arrow' {...Hoverable}>
                            <motion.svg
                                key="4"
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                variants={arrowAnimation}
                                width="108" height="148" fill="none" xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle cx="54" cy="94" r="53.5" stroke="black" />
                                <path d="M55 1C55 0.447715 54.5523 -2.41411e-08 54 0C53.4477 2.41411e-08 53 0.447715 53 1L55 1ZM54 104L59.7735 94L48.2265 94L54 104ZM53 1L53 95L55 95L55 1L53 1Z" fill="black" />
                            </motion.svg>
                        </div>
                    </span>
                </div>

                <div className='sideLine'>
                    <RightLine />
                </div>
            </div>

            <div className='description'>
                <h1>
                    <span>ABOUT THIS</span>
                    <div className='line'></div>
                    <div className='break'></div>
                    <span>PROJECT</span>
                </h1>


                <div className='bio'>
                    <Carousel
                        renderThumbs={() => { }}
                        showStatus={false}
                        renderIndicator={(onClickHandler, isSelected, index, label) => {
                            if (isSelected) {
                                return (
                                    <li style={{
                                        background: 'rgb(20,20,20)',
                                        width: 10,
                                        height: 10,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            } else {
                                return (
                                    <li style={{
                                        background: 'rgb(40,40,40)',
                                        width: 8,
                                        height: 8,
                                        display: 'inline-block',
                                        margin: '8px 8px',
                                        padding: '4px',
                                        borderRadius: '8px'
                                    }}></li>
                                )
                            }
                        }}
                        className="black"
                    >
                        <div>
                            <img src="/images/gallery/cashflow_dashboard.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/cashflow_transfer.png"></img>
                        </div>
                        <div>
                            <img src="/images/gallery/cashflow_login.png"></img>
                        </div>
                    </Carousel>

                    <div className='text'>
                        <p>CashFlow is a sophisticated full-stack banking and eCommerce application, meticulously crafted using Next.js and MongoDB. Initially commissioned as a reward system for a school club, it has evolved into a comprehensive platform where students can manage and utilize simulated digital assets.</p>

                        <p>Key features include robust banking functionalities, such as making and approving transfer requests, with balances and recent activities displayed on user dashboards. The application employs JWT for secure authentication and API route verification, ensuring data integrity and security. Backend processes are seamlessly managed with real-time data storage in MongoDB.</p>

                        <p>The latest enhancement is a fully functional auction feature, enabling users to post and bid on items within the application. This dynamic marketplace environment allows users to leverage their digital assets to participate in auctions, fostering engagement and interactive learning about market dynamics. The auction system includes real-time bidding, automated notifications for bid status, and secure transactions, all seamlessly integrated to enhance the overall user experience.</p>

                        <p className='links'>
                            <a {...Hoverable} href="https://cashflow.alvinqz.com" target="_blank">Live Site (Credentials: guest_1:password, guest_2:password)</a>
                            <span> | </span>
                            <a {...Hoverable} href="https://gitlab.com/alvinzhengq/cashflow" target="_blank">Source Code</a><br />
                        </p>
                    </div>
                </div>
            </div>

            <Footer />
        </div>
    );
}

export default CashFlow;